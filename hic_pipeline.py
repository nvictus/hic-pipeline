from __future__ import division
from os import path
import subprocess
import logging, logging.config
import sys
import os

import yaml
from mirnylib.genome import Genome
from mirnylib.h5dict import h5dict
from mirnylib.numutils import completeIC
from hiclib.mapping import iterative_mapping, parse_sam
from hiclib.fragmentHiC import HiCdataset
from hiclib.binnedData import binnedData
from hiclib.highResBinnedData import HiResHiC
from config import *

logging.config.dictConfig(LOGGING)
logger = logging.getLogger(__name__)

def calculate_step(read_length, proposed_start, proposed_stepsize=10, max_steps=4):
    """
    Return the initial read length and step size for iterative mapping based on 
    the actual length of the reads and proposed initial length

    """
    remain = read_length - proposed_start

    if remain < 0.6*proposed_stepsize:
        start = read_length
        stepsize = 100
    else:
        num_steps = int(round(remain / float(proposed_stepsize)))
        num_steps = min(max(1, num_steps), max_steps)
        stepsize = remain / num_steps
        start = read_length - num_steps*stepsize
    
    return start, stepsize

def map_paired_end_reads(series_name, exp_name, rep_name, sra_file, i, enzyme_name, genome_db):
    rep_fullname = '{}-{}-{}'.format(series_name, exp_name, rep_name)
    sra_fullpath = path.join(READS_DIR, rep_fullname, sra_file)  
    out_fullname = rep_fullname+'-L{}'.format(i+1)

    logger.debug('Mapping reads from {}'.format(sra_file))

    logger.debug('Getting read length parameters')
    cmd = "{} -Z {} | head -n 2 | tail -n 1 | wc -c".format(FASTQDUMP_PATH, sra_fullpath)
    out = subprocess.check_output(cmd, shell=True)
    read_length = (int(out.strip()) - 1)/2
    start, stepsize = calculate_step(read_length, 25)

    # map the reads from each end --> sam/bam files
    # fastq files consist of sequences with the two ends joined together
    logger.debug('Mapping side #1...')
    sam1_path = path.join(MAPPED_DIR, out_fullname+'_1.bam')
    iterative_mapping(
        bowtie_path=BOWTIE_PATH, 
        bowtie_index_path=BOWTIE_INDEX_BASE,
        bowtie_flags=" --very-sensitive",
        bash_reader="{} -Z".format(FASTQDUMP_PATH),
        fastq_path=sra_fullpath,
        out_sam_path=sam1_path,
        min_seq_len=start,
        len_step=stepsize,
        seq_start=0,
        seq_end=read_length
    )
    logger.debug('Mapping side #2...')
    sam2_path = path.join(MAPPED_DIR, out_fullname+'_2.bam')
    iterative_mapping(
        bowtie_path=BOWTIE_PATH,
        bowtie_index_path=BOWTIE_INDEX_BASE,
        bowtie_flags=" --very-sensitive",
        bash_reader="{} -Z".format(FASTQDUMP_PATH),
        fastq_path=sra_fullpath,
        out_sam_path=sam2_path,
        min_seq_len=start,
        len_step=stepsize,
        seq_start=read_length,
        seq_end=2*read_length
    )

    # parse the mapped read sam/bam files from both ends into an hdf5 file
    logger.debug('Parsing bam files to hdf5...')
    mapped_path = path.join(MAPPED_DIR, out_fullname+'-mapped.h5')
    lib_h5dict = h5dict(mapped_path)
    parse_sam(
        sam_basename1=sam1_path,
        sam_basename2=sam2_path,
        out_dict=lib_h5dict,
        genome_db=genome_db,
        enzyme=enzyme_name
    )

    logger.debug('Mapping complete.')

def combine_lanes(series_name, exp_name, rep_name, lanes, enzyme_name, genome_db, in_memory=False):
    rep_fullname = '{}-{}-{}'.format(series_name, exp_name, rep_name)
    merged_path = path.join(MAPPED_DIR, rep_fullname+'-merged.h5')

    lane_fullnames = []
    for i in lanes:
        lane_fullnames.append(
            path.join(MAPPED_DIR, rep_fullname+'-L{}'.format(i)))

    # Parse mapped files
    tmp_paths = []
    for lane_fullname in lane_fullnames:
        in_path = path.join(MAPPED_DIR, lane_fullname+'-mapped.h5')
        stats_path = path.join(LOG_DIR, lane_fullname+'-stats.h5')
        tmp_path = path.join(MAPPED_DIR, lane_fullname+'-parsed.h5')
        tmp_paths.append(tmp_path)
        #out_path = '%s-refined.h5'%lane_fullname

        if in_memory:
            dset = HiCdataset(
                "parsed_inmemory",
                genome=genome_db,
                maximumMoleculeLength=500,
                inMemory=True)
            dset.parseInputData(dictLike=in_path, enzymeToFillRsites=enzyme_name)
            dset.save(tmp_path)
            dset.printMetadata(saveTo=stats_path)
        else:
            dset = HiCdataset(
                tmp_path,
                genome=genome_db,
                maximumMoleculeLength=500,
                mode='w')
            dset.parseInputData(dictLike=in_path, enzymeToFillRsites=enzyme_name)
            dset.printMetadata(saveTo=stats_path)

    # Merge parsed files
    dsetM = HiCdataset(merged_path,
        genome=genome_db,
        mode='w')
    dsetM.merge(tmp_paths)

    # Delete parsed files
    for tmp_path in tmp_paths:
        os.remove(tmp_path)

def apply_filters(series_name, exp_name, rep_name, genome_db):
    rep_fullname = '{}-{}-{}'.format(series_name, exp_name, rep_name)
    merged_path = path.join(MAPPED_DIR, rep_fullname+'-merged.h5')
    refined_path = path.join(MAPPED_DIR, rep_fullname+'-refined.h5')
    stats_path = path.join(LOG_DIR, rep_fullname+'-stats.h5')

    # Apply filters to merged replicate data
    dsetR = HiCdataset(refined_path,
        genome=genome_db,
        mode='w')
    dsetR.load(merged_path)
    #-------------Set of filters applied -------------
    dsetR.filterRsiteStart(offset=5)
    dsetR.filterDuplicates()
    #dsetR.save(out_name+".dat")
    dsetR.filterLarge()
    dsetR.filterExtreme(cutH=0.005, cutL=0)
    #-------------End set of filters applied----------

    dsetR.writeFilteringStats()
    dsetR.printMetadata(saveTo=stats_path)

def combine_replicates(series_name, exp_name, rep_names, genome_db):
    exp_fullname = '{}-{}'.format(series_name, exp_name)
    rep_paths = []
    for rep_name in rep_names:
        rep_fullname = '{}-{}'.format(exp_fullname, rep_name)
        rep_paths.append(
            path.join(MAPPED_DIR, rep_fullname+'-refined.h5'))
    merged_path = path.join(MAPPED_DIR, exp_fullname+'-merged.h5')
    stats_path = path.join(LOG_DIR, exp_fullname+'-stats.h5')

    dset = HiCdataset(merged_path, genome=genome_db)
    dset.merge(rep_paths)
    dset.printMetadata(saveTo=stats_path)

def correct_full(basename, resolution, genome_db):
    bd = binnedData(resolution, genome_db)
    bd.simpleLoad(basename+'.h5', 'MyDataset')

    # Apply filters
    # -------------
    # remove contacts detected between loci in the same bin
    bd.removeDiagonal()
    # remove bins that are less than half sequenced
    bd.removeBySequencedCount(0.5)  #decrease at lower res
    # remove bottom 1% of regions with lowest coverage
    bd.removePoorRegions(cutoff=1)  #increase at lower res
    # removes standalone groups of bins
    bd.removeStandalone()
    # truncate the top 0.05% of interchromosomal contact counts (PCR blowouts)
    bd.truncTrans(high=0.0005)

    # Do iterative correction
    # -----------------------
    bd.iterativeCorrectionWithoutSS()

    bd.export('MyDataset', path.join(BINNED_DIR, basename+'-corrected.h5'))

def correct_bychr(basename, resolution, genome_db, include_trans=False):
    if include_trans:
        # Init binned data object stored in HDD in storage file
        bd = HiResHiC(genome_db, resolution, 
            storageFile=basename+'-corrected.h5')
        # Load hi-res by-chromosome data
        bd.loadData(dictLike=basename+'.h5', mode="all")    

        # Do iterative correction and saves changes to hdf5
        bd.removeDiagonal()
        bd.removePoorRegions()
        bd.iterativeCorrection()
    else:
        dset = h5dict(basename+'.h5', 'r')
        out = h5dict(basename+'-corrected.h5', 'w')
        chrms = [i for i in dset.keys() if len(set(i.split()))==1]
        for chrm in range(len(chrms)):
            hm = dset['{0} {0}'.format(chrm)]
            hmc, bias = completeIC(hm, returnBias=True)
            out['{0} {0}'.format(chrm)] = hmc
            out['bias_{}'.format(chrm)] = bias



##
# Single-experiment processing workflow
##
def do_mapping(series_name, experiment, genome_db):
    """
    Output:
        For each sra file, generates:
            - one or more pairs of bam files
            - one hdf5 file [*-{rep}-L{#}-mapped.h5]

    """
    exp_name = experiment['name']
    enzyme_name = experiment['enzyme']
    for rep in experiment['replicates']:
        rep_name = rep['name'] 
        for i, sra_file in enumerate(rep['files']):
            map_paired_end_reads(series_name, exp_name, rep_name, sra_file, i, enzyme_name, genome_db)


def do_merging(series_name, experiment, genome_db):
    """
    Output:
        For each replicate:
            - two hdf5 files [*-{rep}-merged.h5] and [*-{rep}-refined.h5]
        For the whole experiment: 
            - one hdf5 file [*-merged.h5]

    """
    # Which experiment to aggregate
    exp_name = experiment['name']
    enzyme_name = experiment['enzyme']

    # merge and filter the mapped lane data for each replicate
    for rep in experiment['replicates']:
        rep_name = rep['name']
        lanes = range(1, len(rep['files'])+1)
        combine_lanes(series_name, exp_name, rep_name, lanes, enzyme_name, genome_db) #--> -merged.h5, stats
        apply_filters(series_name, exp_name, rep_name, genome_db) # --> -refined.h5

    # merge the replicates
    rep_names = [x['name'] for x in experiment['replicates']]
    combine_replicates(series_name, exp_name, rep_names, genome_db) # --> -merged.h5, stats


def do_binning(series_name, experiment, genome_db):
    """
    Output:
        hdf5 files containing heatmaps

    """
    exp_name = experiment['name']
    exp_fullname = '{}-{}'.format(series_name, exp_name)

    # build uncorrected heatmaps for the whole experiment
    dset = HiCdataset(path.join(MAPPED_DIR, exp_fullname+'-merged.h5'), genome=genome_db)
    for kb_res in KB_WHOLE:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-full'.format(kb_res))
        dset.saveHeatmap(basename+'.h5', kb_res*1000)

    for kb_res in KB_BYCHR:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-parts.all'.format(kb_res))
        dset.saveByChromosomeHeatmap(basename+'.h5', kb_res*1000)

    for kb_res in KB_HIRES:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-parts.cis'.format(kb_res))
        dset.saveHiResHeatmapWithOverlaps(basename+'.h5', kb_res*1000)


def do_correcting(series_name, experiment, genome_db):
    """
    Output:
        hdf5 files containing corrected heatmaps
        
    """
    exp_name = experiment['name']
    exp_fullname = '{}-{}'.format(series_name, exp_name)

    for kb_res in KB_WHOLE:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-full'.format(kb_res))
        correct_full(basename, kb_res*1000, genome_db) 

    for kb_res in KB_BYCHR:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-parts.all'.format(kb_res))
        correct_bychr(basename, kb_res*1000, genome_db, include_trans=True) 

    for kb_res in KB_HIRES:
        basename = path.join(BINNED_DIR, exp_fullname+'-{}kb-parts.cis'.format(kb_res))
        correct_bychr(basename, kb_res*1000, genome_db)


if __name__=='__main__':

    if not sys.argv:
        raise RuntimeError("Must provide one or more experiment names.")

    with open('dataset.yml') as f:
        data = yaml.load(f)
    series = data['series']
    experiments = [e for e in series['experiments'] if e['name'] in sys.argv]

    genome_db = Genome(GENOME_DIR, readChrms=['#', 'X']) #Y, M

    for experiment in experiments:
        logger.info('Current experiment: {}'.format(experiment['name']))

        logger.info('1. Iterative mapping')
        do_mapping(series['name'], experiment, genome_db)

        logger.info('2. Merge data from lanes, filter, and merge replicates')
        do_merging(series['name'], experiment, genome_db)

        logger.info('3. Bin the data into heatmaps')
        do_binning(series['name'], experiment, genome_db)

        logger.info('4. Iterative correction')
        do_correcting(series['name'], experiment, genome_db)
