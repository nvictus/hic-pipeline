# hiclib-pipeline #

Run the full `hiclib` pipeline on a Hi-C experiment.

4 main tasks:

1. _mapping_
    - iteratively maps paired reads to a reference genome using `bowtie2`
    - raw reads are stored under `./reads`
    - mapped reads are stored under `./mapped`
2. _merging_
    - filters and combines all sequencing lane data for each replicate then combines data from all replicates of an experiment
    - merged datasets are stored under `./mapped`
3. _binning_
    - generates raw heatmaps at desired resolutions
    - heatmap datasets are stored under `./binned`
4. _correcting_
    - does refinement and iterative correction on heatmaps
    - heatmap datasets are stored under `./binned`


### Requires ###

- [hiclib](https://bitbucket.org/mirnylab/hiclib) and dependencies
- PyYAML

### Setup ###

---

- See the example input metadata in `dataset.yml`.
- See the workflow configuration variables in `config.py`.

---

0\. Make sure you have `bowtie2` and `fastq-dump` binaries on your path or specify their path in `config.py`. (fastq-dump is part of the [SRA Toolkit](http://www.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=software))

1\. Download human genome assembly by chromosome in fasta format. Assembly hg19 can be found [here](http://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/). You will need to build an index file with `bowtie2-build`.

```
$ bowtie2-build chr1.fa,chr2.fa,chr3.fa,chr4.fa,chr5.fa,chr6.fa,chr7.fa,chr8.fa,chr9.fa,chr10.fa,chr11.fa,chr12.fa,chr13.fa,chr14.fa,chr15.fa,chr16.fa,chr17.fa,chr18.fa,chr19.fa,chr20.fa,chr21.fa,chr22.fa,chrX.fa,chrY.fa,chrM.fa index
```

2\. Declare your input metadata in a yaml file. Input files associated with a sequencing lane (e.g. SRR400260.sra) are annotated with a _series_ name, an _experiment_ name, a _replicate_ name and a file name. The necessary keys to include in the yaml file are:

- `series`: record with keys `name`, `experiments`
- `experiments`: list of records with keys `name`, `enzyme`, and `replicates`
- `replicates`: list of records with keys `name`, `files`
- `files`: list of file names

3\. Put your raw input data is under `./reads`, grouped by replicate.

Files for the same replicate should appear in a folder named:

    ./reads/<series_name>-<experiment_name>-<replicate_name>/

### Run ###

	$ python hic_pipeline.py <experiment_name1>...

