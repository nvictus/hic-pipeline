#!/bin/bash

geo_download() {
	wget -P $1 -nH -e robots=off --cut-dirs=9 --reject="index.html" --recursive --relative $2
}

urls=$"Jin2013-H1-R1 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX212/SRX212177
Jin2013-IMR90-R1 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX212/SRX212172
Jin2013-IMR90-R2 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX212/SRX212173
Jin2013-IMR90-R3 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX294/SRX294948
Jin2013-IMR90-R4 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX294/SRX294949
Jin2013-IMR90-R5 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX294/SRX294950
Jin2013-IMR90-R6 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX294/SRX294951"

IFS=$"
"
list=($urls)
unset IFS

for pair in "${list[@]}"
do
	arr=($pair)
	name=${arr[0]}
	url=${arr[1]}
	geo_download $name $url
done