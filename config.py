from os import path

__all__ = ['KB_WHOLE', 'KB_BYCHR', 'KB_HIRES', 'FASTQDUMP_PATH', 'BOWTIE_PATH', 
    'BOWTIE_INDEX_BASE', 'GENOME_ASSEMBLY', 'GENOME_DIR', 'READS_DIR', 
    'MAPPED_DIR', 'BINNED_DIR', 'LOG_DIR', 'LOGGING']

# Binning resolutions
KB_WHOLE = [200, 500, 1000, 2000]
KB_BYCHR = [40, 100]
KB_HIRES = [10, 20]

# Location of tools
FASTQDUMP_PATH = '../../bin/fastq-dump'
BOWTIE_PATH ='../../bin/bowtie2'

# Genome data
GENOME_ASSEMBLY = 'hg19'
GENOME_DIR = path.join('../../genomes', GENOME_ASSEMBLY)
BOWTIE_INDEX_BASE = path.join('../../genomes', GENOME_ASSEMBLY, 'index')

# I/O locations
curr_dir = path.dirname(path.realpath(__file__))
READS_DIR = path.join(curr_dir, 'reads')
MAPPED_DIR = path.join(curr_dir, 'mapped')
BINNED_DIR = path.join(curr_dir, 'binned')
LOG_DIR  = path.join(curr_dir, 'logs')

# Logging configuration
LOGGING = {
    'version': 1,            
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': path.join(LOG_DIR, 'debug.log'),
        },
    },
    'loggers': {
        '': {                  
            'handlers': ['default'],        
            'level': 'INFO',  
            'propagate': True,
        },
    }
}
